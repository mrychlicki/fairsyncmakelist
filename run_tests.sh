# fairsyn
cd mascot/fairsyn
mkdir build
rm -R ./build/*
cd build
cmake ..
make
./test/poorfairsyn_test
cd ../..

# mascot
mkdir build
rm -R ./build/*
cd build
cmake ..
make
./bin/poorfairsyn_test
./bin/PoorMascot
cd ..

# install_fairsyn
cd fairsyn/build
sudo make install
cd ../../../

#other_project
cd other_project
mkdir build
rm -R ./build/*
cd build
cmake ..
make
./OtherProject
cd ../..