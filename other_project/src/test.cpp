//
// Created by mrychlicki on 9/8/21.
//
#include "ubdd/CuddUBDD.h"
#include "ubdd/SylvanUBDD.h"
int main() {
    poorfairsyn::PoorCuddUBDD c("other_project");
    c.print_bdd();

    poorfairsyn::PoorSylvanUBDD s("other_project");
    s.print_bdd();
}