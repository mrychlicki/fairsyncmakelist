//
// Created by mrychlicki on 9/8/21.
//
#include "ubdd/CuddUBDD.h"

namespace poorfairsyn {
    void PoorCuddUBDD::print_bdd() {
        std::cout << "Cudd 2.0 Works: " + folder << "\n";
        assert(bdd.CountMinterm(2) == 3);
    }
}
