//
// Created by mrychlicki on 9/8/21.
//
#include "ubdd/SylvanUBDD.h"

namespace poorfairsyn {
    void PoorSylvanUBDD::print_bdd() {
        std::cout << "Sylvan Works: " + folder << "\n";
        assert(bdd.SatCount(2) == 3);
    }
}
