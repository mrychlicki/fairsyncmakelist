//
// Created by mrychlicki on 9/8/21.
//
#pragma once

#include "BaseUBDD.h"

#include "cudd.h"
#include "cuddObj.hh"
#include "dddmp.h"

namespace poorfairsyn {
    class PoorCuddUBDD : public poorfairsyn::PoorBaseUBDD {
    private:
        std::string folder;
        Cudd cudd;
        BDD bdd;
    public:
        PoorCuddUBDD(std::string s) {
            folder = s;
            cudd = Cudd();
            bdd = cudd.bddVar(0) | cudd.bddVar(1);
        }

        void print_bdd();
    };
}
