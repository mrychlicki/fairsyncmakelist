//
// Created by mrychlicki on 9/9/21.
//
#pragma once

#include <iostream>
#include <string>

namespace poorfairsyn {
    class PoorBaseUBDD {
    public:
        virtual void print_bdd() = 0;
    };
}