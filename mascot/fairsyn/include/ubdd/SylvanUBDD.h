//
// Created by mrychlicki on 9/8/21.
//
#pragma once

#include "BaseUBDD.h"

#include "sylvan_obj.hpp"

#include "sylvan_bdd.h"


namespace poorfairsyn {
    class PoorSylvanUBDD : public poorfairsyn::PoorBaseUBDD {
    private:
        std::string folder;
        sylvan::Bdd bdd;
    public:
        PoorSylvanUBDD(std::string s) {
            init_sylvan();
            folder = s;
            bdd = bdd.bddVar(0) | bdd.bddVar(1);
        }

        ~PoorSylvanUBDD() {
            stop_sylvan();
        }

        void print_bdd();

        void init_sylvan() {
            /* initiate the lace work-stealing framework and the sylvan parallel bdd library */
            int dqsize = 100000;
            lace_start(2, dqsize);
            // use at most 1 GB, nodes:cache ratio 2:1, initial size 1/32 of maximum
            sylvan::sylvan_set_limits(1 * 1024 * 1024 * 1024, 1, 5);
            sylvan::sylvan_init_package();
            sylvan::sylvan_init_mtbdd();
        }

        void stop_sylvan() {
            sylvan::sylvan_stats_report(stdout);
            sylvan::sylvan_quit();
            lace_stop();
        }
    };
}