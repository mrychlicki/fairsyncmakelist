//
// Created by mrychlicki on 9/8/21.
//

#include "ubdd/CuddUBDD.h"
#include "ubdd/SylvanUBDD.h"
int main() {
    poorfairsyn::PoorCuddUBDD c("mascot");
    c.print_bdd();

    poorfairsyn::PoorSylvanUBDD s("mascot");
    s.print_bdd();
}